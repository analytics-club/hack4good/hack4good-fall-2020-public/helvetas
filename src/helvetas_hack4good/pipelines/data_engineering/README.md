# Data Engineering pipeline

## Overview

Data cleaning and features engineering for Helvetas dataset.

## Pipeline inputs

### `helvetas_data`

|      |                    |
| ---- | ------------------ |
| Type | `pandas.DataFrame` |
| Description | Raw data Helvetas |

### `params:remove_na`

|      |                    |
| ---- | ------------------ |
| Type | `bool` |
| Description | Remove observations with NA values |

### `params:require_birthyear` !! Not used anymore, implemented in split_label

|      |                    |
| ---- | ------------------ |
| Type | `bool` |
| Description | Filter out samples without birth year. Return labeled dataset |

### `params:string_to_int`

|      |                    |
| ---- | ------------------ |
| Type | `bool` |
| Description | Transform string variables to int |

### `params:verbose`

|      |                    |
| ---- | ------------------ |
| Type | `bool` |
| Description | Verbose option clean data function |

## Pipeline outputs

### `preprocessed_data`

|      |                    |
| ---- | ------------------ |
| Type | `pandas.DataFrame` |
| Description | Preprocessing data steps |

