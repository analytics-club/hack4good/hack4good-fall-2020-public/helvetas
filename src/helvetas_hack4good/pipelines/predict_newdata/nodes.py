"""
Nodes to read in and preprocess raw data
"""
# %%
from typing import Tuple
import pandas as pd
import numpy as np
from datetime import datetime
import category_encoders as ce
from missingpy import MissForest
from pandas.api.types import is_numeric_dtype


def clean_helvetas_data(data: pd.DataFrame, verbose: bool = True) -> pd.DataFrame:
    """
    Reads in the Helvetas data file and performs some basic cleaning.
    Type transformation, date features are transform to a numerical values (decimal date),
    quelle category is simplified in six categories, first name cleaning and replace outliers by NaN.


    Parameters
    ----------
    data : pd.DataFrame
        The raw Helvetas input data.
    verbose : bool
        Whether to print some information on the data size, by default True

    Returns
    -------
    pd.DataFrame
        The cleaned data
    """

    if verbose:
        print("{:40}: {:d}".format("Helvetas data - Original number of entries", len(data.index)))

    data = data.copy(deep=True)

    # Convert the date-columns
    data = convert_date_to_datetime(data, ['andat', 'first_donation', 'last_donation'])

    # Convert numeric columns to numeric
    number_columns = ['adrnum', 'gebjah', 'plz', 'qty_donations', 'LTV_donations', 'AVG_donation']
    for col in number_columns:
        data[col] = pd.to_numeric(data[col], errors='coerce')  # Convert to numeric

    # Make string columns explicit strings and remove leading and trailing spaces in string columns
    string_columns = ['quelle', 'sprachcd', 'gescode', 'land', 'ort', 'kanton']
    for col in string_columns:
        data[col] = data[col].astype(str).replace('nan', np.nan)
        data[col] = data[col].str.strip()  # Or remove special characters as in name?

    # Clean name field
    name = data.vname.str.split(" ", expand=True)
    # choose longest
    name.iloc[:, 1] = name.iloc[:, 1].where(name.iloc[:, 1].notnull(), "")
    name["vname"] = name.iloc[:, 0].where(name.iloc[:, 0].str.len() >= name.iloc[:, 1].str.len(), name.iloc[:, 1])
    data["vname"] = name.vname.str.lower().str.normalize('NFKD').str.encode(
        'ascii', errors='ignore').str.decode('utf-8').str.replace(r"[^a-zA-Z\d\_]+", "")

    # Convert bool columns
    bool_columns = ['active_member', 'has_been_member']
    for col in bool_columns:
        data[col] = data[col].astype('bool')  # Convert to bool

    # Replace outliers with NA for date values
    data.loc[(data['gebjah'] < 1860) & (data['gebjah'] > 0), 'gebjah'] = np.nan
    data.dropna(subset=['gebjah'], inplace=True)
    date_columns = ['andat', 'first_donation', 'last_donation']
    for col in date_columns:
        if any(data[col].dt.year < 1950):
            print("{}".format("Year is older than 1950, replacing by NaN"))
        data.loc[data[col].dt.year < 1950, col] = pd.NaT  # Equivalent to nan for datetime

    # quelle feature (create new column == "quelle_cat")
    data["quelle_cat"] = data["quelle"].apply(lambda x: str(x))  # NaN to 'nan'
    data["quelle_cat"] = data["quelle_cat"].apply(lambda x: "f" if x[0] == "f" else x)  # starts with f -- "f" category
    categories = ["m", "FTML", "corr", "mmove", "f"]
    data["quelle_cat"] = data["quelle_cat"].apply(
        lambda x: "other" if x not in categories else x)  # if not in categories, other
    data["quelle_cat"].replace({"f": "foreign", "m": "manual", "FTML": "digital", "corr": "external"}, inplace=True)

    # change dates to decimal dates
    data = map_datetime_to_decimal(data, 'andat')
    data = map_datetime_to_decimal(data, 'first_donation')
    data = map_datetime_to_decimal(data, 'last_donation')

    # Remove duplicates
    data.drop_duplicates()

    if verbose:
        print("{:40}: {:d}".format("Helvetas data - Final number of entries ", len(data.index)))

    return data


def clean_postcodes_data(plz_data: pd.DataFrame, geo_data: pd.DataFrame, verbose: bool = True) -> pd.DataFrame:
    """
    Extract the relevant columns from the post code  and geographical Swiss data and do basic cleaning.

    Parameters
    ----------
    plz_data : pd.DataFrame
        The uncleaned pandas data frame including post codes and BFS (municipality numbers)
    geo_data : pd.DataFrame
        The uncleaned pandas data frame including geographical information by municipality
    verbose : bool, optional
        Whether to print additional information, by default True
    Returns
    -------
    pd.DataFrame
        Dataframe with BFS numbers ("municipality_number") and postal codes ("plz")
    """

    plz_data = plz_data[["PLZ", "BFS-Nr", "Kantonskürzel", "E", "N"]]
    plz_data.columns = ["plz", "municipality_number", "kanton", "E", "N"]
    # We do not add LI (Liechtenstain) as a canton
    plz_data = plz_data[plz_data.kanton != "LI"]

    geo_data = geo_data.drop(0)
    geo_data.columns = ["municipality_number", "municipality_name",
                        "kanton_number", "kanton", "district_number", "district_name", "region"]

    postcodes_data = plz_data.merge(geo_data, on=["municipality_number", "kanton"], how="left")

    with pd.option_context('mode.use_inf_as_na', True):
        for col in ["plz", "municipality_number"]:
            postcodes_data[col] = pd.to_numeric(postcodes_data[col], errors="coerce", downcast="integer")

    postcodes_data.drop_duplicates(subset=['plz', 'municipality_number',
                                           'kanton_number', 'district_number'], inplace=True)

    if verbose:
        print("External data - Number of postal codes:", len(postcodes_data["plz"].unique()))
        print("External data - Number of municipality codes:", len(postcodes_data["municipality_number"].unique()))
        print("External data - Number of districts:", len(postcodes_data["district_number"].unique()))
        print("External data - Number of cantons:", len(postcodes_data["kanton_number"].unique()))

    return postcodes_data


def clean_population_data(raw_data: pd.DataFrame, verbose: bool = True) -> pd.DataFrame:
    """
    Extract the relevant columns from the municipality resoluted population data and do basic cleaning.
    Also computes the average birth year in a municipality (municipality_year) and the most common
    birth year (municipality_maxyear). Ages above 100 are considered as 100.

    Parameters
    ----------
    raw_data : pd.DataFrame
        Data frame with geographically resoluted population data for Switzerland.
    verbose : bool
        Whether to print additional information, by default True

    Returns
    -------
    pd.DataFrame
        Data frame with population and birth year by municipality.
    """
    data = raw_data.copy(deep=True)
    # Municipality number is integer in Region column
    data.insert(1, "municipality_number", data['Region'].apply(lambda x: ''.join(filter((str.isdigit), str(x)))))
    data.drop(columns="Region", inplace=True)  # Drop region column
    data.rename(columns={"Total": "municipality_pop"}, inplace=True)

    # Drop rows with empty fields and store population as integers
    data.replace('', np.nan, inplace=True)
    data.dropna(inplace=True)
    data["municipality_pop"] = data["municipality_pop"].astype(int)
    data["municipality_number"] = pd.to_numeric(data["municipality_number"], errors="coerce", downcast="integer")

    # Compute expected birth year
    # All ages beyond 100 are in the 100th column and hence assumed to be 100 here
    ages = np.arange(0, 101)
    data.rename(columns={'100 und mehr': 100}, inplace=True)
    data["municipality_year"] = 2019-(ages * data.loc[:, ages]).divide(data.loc[:,
                                                                                ages].sum(axis=1), axis=0).sum(axis=1)
    data["municipality_maxyear"] = 2019-(data.loc[:, ages].idxmax(axis="columns"))

    if verbose:
        print("External data - Number of municipalities with population data:",
              len(data["municipality_number"].unique()))
        print("External data - Average birth year in Switzerland:", round(data["municipality_year"].mean()))

    return data


def clean_income_data(raw_data: pd.DataFrame, verbose: bool = True) -> pd.DataFrame:
    """
    Extract the relevant columns from the municipality resoluted income data and do basic cleaning.

    Parameters
    ----------
    raw_data : pd.DataFrame
        Data frame with geographically resoluted income data for Switzerland.
    verbose : bool
        Whether to print additional information, by default True

    Returns
    -------
    pd.DataFrame
        Data frame with municipality numbers ("municipality_number"), number of tax payers in the municipality
        ("mun_n_taxpayers") and total yearly income per municipality in 1000 CHF ("mun_income_kCHF")
    """
    data = raw_data.copy(deep=True)
    data.drop(index=1, axis=0, inplace=True)  # Column names in French
    data.drop(index=0, axis=0, inplace=True)  # Whole switzerland info = sum of all municipalities
    data.columns = [col.replace('\n', '') for col in data.columns]  # Replace new lines
    data = data[["Kantons-nummer", "Gemeinde-nummer", "Steuerpflichtige", "ReinesEinkommen"]].copy()

    data.columns = ["kanton_number", "municipality_number", "mun_n_taxpayers", "mun_income_kCHF"]
    data["municipality_number"] = pd.to_numeric(data["municipality_number"], errors="coerce", downcast="integer")

    if verbose:
        print("External data - Number of municipalities with income data:", len(data["municipality_number"].unique()))

    return data


def clean_wealth_data(raw_data: pd.DataFrame, verbose: bool = True) -> pd.DataFrame:
    """
    Extract the relevant columns from the canton resoluted wealth data and do basic cleaning.

    Parameters
    ----------
    raw_data : pd.DataFrame
        Data frame with geographically resoluted wealth data for Switzerland.
    verbose : bool
        Whether to print additional information, by default True

    Returns
    -------
    pd.DataFrame
        The canton name abbreviations ("kanton"), number of tax payers in the canton
        ("kan_n_taxpayer") and total wealth per canton in million CHF ("kan_wealth_Mchf")
    """
    data = raw_data[["Kanton", "Nombres absolus", "en millions de francs"]].copy(deep=True)
    data.drop(index=30, inplace=True)  # Last row
    data.drop(index=27, inplace=True)  # Entire Switzerland
    data.dropna(axis=0, how="all", inplace=True)  # drop empty rows
    data.columns = ["kanton", "kan_n_taxpayer", "kan_wealth_Mchf"]
    data["kan_n_taxpayer"] = data["kan_n_taxpayer"].astype(int)

    # Replace canton names by canton abbreviations
    # https://www.parlament.ch/de/%C3%BCber-das-parlament/parlamentsw%C3%B6rterbuch/abkuerzungen on Oct 30 2020
    # Some of the canton names were translated or abbreviated to fit with the data set notation
    canton_abbr = {
        "Aargau": "AG",
        "Appenzell I.Rh.": "AI",
        "Appenzell A.Rh.": "AR",
        "Bern": "BE",
        "Basel-Landschaft": "BL",
        "Basel-Stadt": "BS",
        "Fribourg": "FR",
        "Genève": "GE",
        "Glarus": "GL",
        "Graubünden": "GR",
        "Jura": "JU",
        "Luzern": "LU",
        "Neuchâtel": "NE",
        "Nidwalden": "NW",
        "Obwalden": "OW",
        "St. Gallen": "SG",
        "Schaffhausen": "SH",
        "Solothurn": "SO",
        "Schwyz": "SZ",
        "Thurgau": "TG",
        "Ticino": "TI",
        "Uri": "UR",
        "Vaud": "VD",
        "Valais": "VS",
        "Zug": "ZG",
        "Zürich": "ZH"
    }

    # Remove leading or trailing spaces
    data['kanton'] = data['kanton'].str.strip()
    # Replace canton names by abbreviations
    data = data.replace({"kanton": canton_abbr})

    if verbose:
        print("External data - Number of kantons with wealth data:", len(data["kan_wealth_Mchf"].unique()))

    return data


def clean_education_data(raw_data: pd.DataFrame, verbose: bool = True) -> pd.DataFrame:
    """
    Extract the relevant columns from the canton resoluted education data and do basic cleaning.
    Computation of the proportion of the population over 25 years without post-compulsory training (ed0),
    with proffesional education (ed1),  general education (ed2), superior education (ed3) or Haute ecole (ed4) by
    canton.

    Parameters
    ----------
    raw_data : pd.DataFrame
        Data frame with canton resoluted education data for Switzerland.
    verbose : bool
        Whether to print additional information, by default True

    Returns
    -------
    pd.DataFrame
        Data frame with education level population proportion by canton.
    """
    education_data = raw_data.iloc[:, [2, 3, 5, 7, 9, 11, 13]]
    education_data.columns = ["kanton", "plus25", "ed0", "ed1", "ed2", "ed3", "ed4"]
    canton_abbr = {"Zürich": "ZH",
                   "Bern / Berne": "BE",
                   "Luzern": "LU",
                   "Uri": "UR",
                   "Schwyz": "SZ",
                   "Obwalden": "OW",
                   "Nidwalden": "NW",
                   "Glarus": "GL",
                   "Zug": "ZG",
                   "Fribourg / Freiburg": "FR",
                   "Solothurn": "SO",
                   "Basel-Stadt": "BS",
                   "Basel-Landschaft": "BL",
                   "Schaffhausen": "SH",
                   "Appenzell Ausserrhoden": "AR",
                   "Appenzell Innerrhoden": "AI",
                   "St. Gallen": "SG",
                   "Graubünden / Grigioni / Grischun": "GR",
                   "Aargau": "AG",
                   "Thurgau": "TG",
                   "Ticino": "TI",
                   "Vaud": "VD",
                   "Valais / Wallis": "VS",
                   "Neuchâtel": "NE",
                   "Genève": "GE",
                   "Jura": "JU"
                   }

    education_data = education_data.replace({"kanton": canton_abbr})
    for col in ["ed0", "ed1", "ed2", "ed3", "ed4"]:
        education_data[col] = education_data[col]/education_data.plus25

    if verbose:
        print("External data - Number of kantons with education data:", len(education_data["kanton"].unique()))

    return education_data


def clean_name_data(male_names: pd.DataFrame, female_names: pd.DataFrame, verbose: bool = True) -> pd.DataFrame:
    """
    Basic cleaning of most frequent names in Switzerland data and computation of
    average year (vname_year )and most frequent year by name (vname_maxyear).

    Parameters
    ----------
    raw_data : pd.DataFrame
        Data frame with canton resoluted education data for Switzerland.
    verbose : bool
        Whether to print additional information, by default True

    Returns
    -------
    pd.DataFrame
        Birth year information by name for the 1000 most frequent female and male names in Switzerland.
    """

    males = male_names.copy(deep=True)
    females = female_names.copy(deep=True)
    # Rename first name column
    males.rename(columns={'First name': 'vname'}, inplace=True)
    females.rename(columns={'First name': 'vname'}, inplace=True)
    # Replace accents and remove special characters in names (lower case, no spaces or hyphens
    males["vname"] = males.vname.str.lower().str.normalize('NFKD').str.encode(
        'ascii', errors='ignore').str.decode('utf-8').replace(r"[^a-zA-Z\d\_]+", "")
    females["vname"] = females.vname.str.lower().str.normalize('NFKD').str.encode(
        'ascii', errors='ignore').str.decode('utf-8').replace(r"[^a-zA-Z\d\_]+", "")
    # Add gender information and join
    females.insert(1, 'gescode', 'f')
    males.insert(1, 'gescode', 'm')
    # Add empty years male dataset
    males.insert(3, 'J_1914', '*')
    males.insert(4, 'J_1915', '*')
    males.insert(5, 'J_1916', '*')
    # Join datasets
    data = pd.concat([females, males])
    data.reset_index(drop=True, inplace=True)
    # Rename years
    j_years = data.filter(like='J_')
    years = j_years.rename(columns=lambda x: int(x[2:])).columns
    data.rename(columns=dict(zip(j_years, years)), inplace=True)
    data.rename(columns=dict(zip(j_years, years)), inplace=True)
    # Replace * (no data) with 0 values
    data.replace('*', 0, inplace=True)

    if verbose:
        print("External data - Number of names with birth year information",
              len(data.drop_duplicates(subset=["vname", "gescode"])))

    return data


def clean_external_data(plz_data: pd.DataFrame, geo_data: pd.DataFrame, population_data: pd.DataFrame, income_data: pd.DataFrame,
                        wealth_data: pd.DataFrame, education_data: pd.DataFrame, male_names: pd.DataFrame, female_names: pd.DataFrame,
                        verbose: bool = True) -> Tuple[pd.DataFrame, pd.DataFrame, pd.DataFrame, pd.DataFrame, pd.DataFrame, pd.DataFrame]:
    """
    Clean all external data sets.

    Parameters
    ----------
    plz_data : pd.DataFrame
        The unclean postcodes data.
    geo_data : pd.DataFrame
        The unclean geographical data.
    population_data : pd.DataFrame
        The unclean population data.
    income_data : pd.DataFrame
        The unclean income data.
    education_data : pd.DataFrame
        The unclean income data.
    male_names: pd.Dataframe
        Male first name data.
    female_names: pd.Dataframe
        Female first name data.
    verbose : bool, optional
        Whether to print additional information, by default True

    Returns
    -------
    Tuple[pd.DataFrame, pd.DataFrame]
        The cleaned demographics data (by postal code) and names data (by first name)
    """
    cleaned_postcodes = clean_postcodes_data(plz_data, geo_data, verbose)
    cleaned_population = clean_population_data(population_data, verbose)
    cleaned_income = clean_income_data(income_data, verbose)
    cleaned_wealth = clean_wealth_data(wealth_data, verbose)
    cleaned_education = clean_education_data(education_data, verbose)

    cleaned_names = clean_name_data(male_names, female_names, verbose)

    return cleaned_postcodes, cleaned_population, cleaned_income, cleaned_wealth, cleaned_education, cleaned_names


def transform_external_data(cleaned_postcodes: pd.DataFrame, cleaned_population: pd.DataFrame, cleaned_income: pd.DataFrame,
                            cleaned_wealth: pd.DataFrame, cleaned_education: pd.DataFrame, cleaned_names: pd.DataFrame,
                            verbose: bool = True) -> Tuple[pd.DataFrame, pd.DataFrame]:
    """
    Transform external data sets, compute population and birth year values for canton and district and
    income and wealth data information per capita and per tax payer.
    Merge all demographic information in one dataset with unique rows for each postal code.

    Parameters
    ----------
    cleaned_postcodes : pd.DataFrame
        The clean postcodes and geographical data.
    cleaned_population : pd.DataFrame
        The clean population data.
    cleaned_income : pd.DataFrame
        The clean income data.
    cleaned_wealth : pd.DataFrame
        The clean wealth data.
    cleaned_education : pd.DataFrame
        The clean income data.
    cleaned_names: pd.Dataframe
        The clean names data.
    verbose : bool, optional
        Whether to print additional information, by default True

    Returns
    -------
    Tuple[pd.DataFrame, pd.DataFrame]
        The cleaned demographics data (by postal code) and names data (by first name)
    """

    # Demographics information
    # 1. Population and birth year information by canton, district and municipality
    cleaned_demographics = cleaned_postcodes.merge(cleaned_population, how="left")
    cleaned_demographics.drop_duplicates(subset=["plz", "kanton"], inplace=True)
    cleaned_demographics['district_pop'] = cleaned_demographics['municipality_pop'].groupby(
        cleaned_demographics['district_name']).transform('sum')
    cleaned_demographics['kanton_pop'] = cleaned_demographics['municipality_pop'].groupby(
        cleaned_demographics['kanton']).transform('sum')
    cleaned_demographics['district_year'] = (cleaned_demographics.municipality_year * cleaned_demographics.municipality_pop /
                                             cleaned_demographics.district_pop).groupby(cleaned_demographics['district_name']).transform('sum')
    cleaned_demographics['kanton_year'] = (cleaned_demographics.municipality_year * cleaned_demographics.municipality_pop /
                                           cleaned_demographics.kanton_pop).groupby(cleaned_demographics['kanton']).transform('sum')
    cleaned_demographics["district_maxyear"] = 2019-(cleaned_demographics.loc[:, range(0, 101)].groupby(
        cleaned_demographics['district_name']).transform('sum').idxmax(axis="columns"))
    cleaned_demographics["kanton_maxyear"] = 2019-(cleaned_demographics.loc[:, range(0, 101)].groupby(
        cleaned_demographics['kanton']).transform('sum').idxmax(axis="columns"))
    cleaned_demographics.drop(range(0, 101),  axis="columns", inplace=True)
    # 2. Income data
    cleaned_demographics = cleaned_demographics.merge(cleaned_income, how="left")
    # 3. Wealth data
    cleaned_demographics = cleaned_demographics.merge(cleaned_wealth, how="left")
    # 4. Income and wealth data per capita and per tax payer by canton, district and municipality
    cleaned_demographics["dis_income_kCHF"] = cleaned_demographics['mun_income_kCHF'].groupby(
        cleaned_demographics['district_name']).transform('sum')
    cleaned_demographics["dis_n_taxpayers"] = cleaned_demographics['mun_n_taxpayers'].groupby(
        cleaned_demographics['district_name']).transform('sum')
    cleaned_demographics["kan_income_kCHF"] = cleaned_demographics['mun_income_kCHF'].groupby(
        cleaned_demographics['kanton']).transform('sum')
    cleaned_demographics["mun_income_kCHF_ptp"] = cleaned_demographics["mun_income_kCHF"] / \
        cleaned_demographics["mun_n_taxpayers"]
    cleaned_demographics["mun_income_kCHF_pc"] = cleaned_demographics["mun_income_kCHF"] / \
        cleaned_demographics["municipality_pop"]
    cleaned_demographics["dis_income_kCHF_ptp"] = cleaned_demographics["dis_income_kCHF"] / \
        cleaned_demographics["dis_n_taxpayers"]
    cleaned_demographics["dis_income_kCHF_pc"] = cleaned_demographics["dis_income_kCHF"] / \
        cleaned_demographics["district_pop"]
    cleaned_demographics["kan_income_kCHF_ptp"] = cleaned_demographics["kan_income_kCHF"] / \
        cleaned_demographics["kan_n_taxpayer"]
    cleaned_demographics["kan_income_kCHF_pc"] = cleaned_demographics["kan_income_kCHF"] / \
        cleaned_demographics["kanton_pop"]
    cleaned_demographics["kan_wealth_Mchf_ptp"] = cleaned_demographics["kan_wealth_Mchf"] / \
        cleaned_demographics["kan_n_taxpayer"]
    cleaned_demographics["kan_wealth_Mchf_pc"] = cleaned_demographics["kan_wealth_Mchf"] / \
        cleaned_demographics["kanton_pop"]
    # 5. Education data
    cleaned_demographics = cleaned_demographics.merge(cleaned_education, how="left")

    # Fist name information
    # 1. Create expected year (weighted average)
    # Expected year
    # NOTE If vname_max_year is to be included in the enhanced data set exclude last element of names in calculation of vname_year
    years = cleaned_names.filter(regex=r"[0-9]").columns.astype('float64')
    cleaned_names["vname_maxyear"] = cleaned_names.loc[:, years].idxmax(axis="columns")
    cleaned_names["vname_year"] = (years * cleaned_names.loc[:, years]
                                   ).divide(cleaned_names.loc[:, years].sum(axis=1), axis=0).sum(axis=1)
    cleaned_names.drop_duplicates(subset=["vname", "gescode"], inplace=True)

    # 2. Another approach could be to have one column for each generation,
    # with the probabilities of that name in that generation
    #
    # - gen1, WiederaufbauerInnen: Birthyear before 1952
    # - gen2, Babyboomer: Birthyears 1952-1966
    # - gen3, Generation X: Birthyears 1967-1981
    # - gen4, Millenials: Birthyears 1982 – 1996
    # - gen5, iBrains: Birthyears 1997 - 2011
    # - gen6, Gen Alpha: Birthyears 2012 - Present
    cleaned_names["gen1"] = cleaned_names.loc[:, range(1914, 1952)].sum(
        axis=1).divide(cleaned_names.loc[:, years].sum(axis=1), axis=0)
    cleaned_names["gen2"] = cleaned_names.loc[:, range(1952, 1967)].sum(
        axis=1).divide(cleaned_names.loc[:, years].sum(axis=1), axis=0)
    cleaned_names["gen3"] = cleaned_names.loc[:, range(1967, 1982)].sum(
        axis=1).divide(cleaned_names.loc[:, years].sum(axis=1), axis=0)
    cleaned_names["gen4"] = cleaned_names.loc[:, range(1982, 1997)].sum(
        axis=1).divide(cleaned_names.loc[:, years].sum(axis=1), axis=0)
    cleaned_names["gen5"] = cleaned_names.loc[:, range(1997, 2012)].sum(
        axis=1).divide(cleaned_names.loc[:, years].sum(axis=1), axis=0)
    cleaned_names["gen6"] = cleaned_names.loc[:, range(2012, 2020)].sum(
        axis=1).divide(cleaned_names.loc[:, years].sum(axis=1), axis=0)
    cols = ["vname", "gescode", "vname_year", "vname_maxyear", "gen1", "gen2", "gen3", "gen4", "gen5", "gen6"]
    cleaned_names = cleaned_names[cols]

    if verbose:
        print("External data - Number of final geo and demographic variables: ", cleaned_demographics.shape[1])
        print("External data - Number of final name related variables: ", cleaned_names.shape[1])

    return cleaned_demographics, cleaned_names


def enhance_data(helvetas_data: pd.DataFrame, demographics_data: pd.DataFrame,  names_data: pd.DataFrame, verbose: bool = True) -> pd.DataFrame:
    """
    Enriches the helvetas data with external data based on geographic (post code) information and first name information.

    Parameters
    ----------
    helvetas_data : pd.DataFrame
        The donors data frame to enhance.
    demographics_data : pd.DataFrame
        The data frame containing the demographic information by postal code.
    names_data: pd.DataFrame
        The data frame containing the birth year information by first name and sex.
    verbose : bool
        Whether to print additional information, by default True

    Returns
    -------
    pd.DataFrame
        The enhanced data frame.
    """
    # Enhance with geography info
    data = helvetas_data.copy(deep=True)
    # Since Swiss postcodes are unique in Europe we do not exclude non-swiss entries explicitly
    # Add the municipality number to the helvetas data
    data = data.merge(demographics_data, how="left", on="plz")
    data.kanton_x = data.kanton_x.fillna(data.kanton_y)
    data["kanton"] = data.kanton_x
    data = data.drop(["kanton_x", "kanton_y"], axis="columns")
    # Drop duplicates due to multiplicity plz-municipality
    data.drop_duplicates(subset=['adrnum'], inplace=True)

    if helvetas_data.shape[0] != data.shape[0]:
        "Warning! duplicates have been introduced after enhancement with demographics data."

    if verbose:
        n = sum(helvetas_data.plz.isin(demographics_data.plz))
        print("Enhanced data -  Number of helvetas donors with postal code in demographics data: ",
              n, " (", round(n/helvetas_data.shape[0]*100), "%) donors ")

    # Add first name based age guess
    # 2. Merge with helvetas dataset
    enhanced_data = pd.merge(data, names_data, how="left")

    if helvetas_data.shape[0] != enhanced_data.shape[0]:
        "Warning! duplicates have been introduced after enhancement with names data."

    if verbose:
        n = sum(helvetas_data.vname.isin(names_data.vname))
        print("Enhanced data -  There are ", n, "(", round(n/len(helvetas_data)*100),
              "%) donors with names in the Swiss name statistics")
        print("Enhanced data -  Final number of entries in the dataset: ", len(enhanced_data))

    return enhanced_data


def missing_data(data_missing: pd.DataFrame, missing_action: str, verbose=True) -> pd.DataFrame:
    """
    deleting datapoints(rows) with missing values (NAs) from the dataframe
    operates on all columns expect from the column "quelle"

    Parameters
    ----------
    input:
    data_missing: pd.DataFrame
        dataframe with missing /NA values
    verbose: bool, optional
        Whether to print information about the length of the splitted data frames, by default True.

    Returns:
    ----------
    pd.DataFrame
        The dataframe without NAs in rows, except from the column "quelle"

    """

    df = data_missing.copy(deep=True)

    if missing_action == "drop_median":
        # Remove rows contains na in all columns. Only in labeled data.
        labeled = df[df.gebjah != 0].dropna(axis=0)
        unlabeled = df[df.gebjah == 0]
        for col in unlabeled.columns:
            if col != "gebjah":
                unlabeled[col].fillna(unlabeled[col].median(), inplace=True)
        df = labeled.append(unlabeled)
    else:
        if missing_action == "mean":
            for col in df.columns:
                if col != "gebjah":
                    df[col].fillna(df[col].mean(), inplace=True)

        elif missing_action == "median":
            for col in df.columns:
                if col != "gebjah":
                    df[col].fillna(df[col].median(), inplace=True)

        # elif missing_action == "randomForest":
        # Nice method but too slow and not such a  big difference
        #     imputer = MissForest(max_depth=1, verbose=verbose)
        #     X = df.drop("gebjah", axis="columns")
        #     label = df.gebjah
        #     X_imputed = imputer.fit_transform(X)
        #     df = X_imputed.insert("gebjah", label)

    if verbose:
        print("Maximum number of missing entries in one feature: ", len(
            data_missing) - min(data_missing.drop("gebjah", axis="columns").count()))
        print("Features data -  Final number of entries in the dataset: ", len(df))

    return df


def create_features(data: pd.DataFrame, encoding: str, missing_action: str) -> pd.DataFrame:
    """
    Feature engineering and missing data imputation.

    Parameters
    ----------
    data : pd.DataFrame
        Enhanced data set

    Returns
    -------
    pd.DataFrame
        Dataset with all features
    """
    data = data.copy(deep=True)

    # 1. Categorical variables encoding
    # Using https://contrib.scikit-learn.org/category_encoders/
    columns_to_encode = ["quelle_cat", "sprachcd", "gescode", "kanton", "region"]

    if encoding == "onehot":
        encoder = ce.OneHotEncoder(cols=columns_to_encode, return_df=True)

    elif encoding == "binary_encoder":
        encoder = ce.BinaryEncoder(cols=columns_to_encode, return_df=True)
    # We can add more types of encoders
    data = encoder.fit_transform(data)

    # 2. Feature preselection
    cols_to_drop = ['quelle', 'vname', 'land', 'plz', 'ort', 'andat',
                    'first_donation', 'last_donation', 'municipality_number', 'municipality_name',
                    'kanton_number', 'district_number', 'district_name', 'plus25']
    data.drop(cols_to_drop, axis="columns", inplace=True)
    data.set_index('adrnum', inplace=True)

    # 3. Missing data imputation
    data = missing_data(data, missing_action)

    # 4. Log transformation of numerical values
    cols_to_transform = ['qty_donations', 'LTV_donations', 'AVG_donation', 'district_pop', 'mun_n_taxpayers',
                         'mun_income_kCHF', 'kanton_pop', 'kan_n_taxpayer', 'kan_wealth_Mchf', 'dis_income_kCHF', 'dis_n_taxpayers',
                         'kan_income_kCHF', 'mun_income_kCHF_ptp', 'mun_income_kCHF_pc', 'dis_income_kCHF_ptp', 'dis_income_kCHF_pc',
                         'kan_income_kCHF_ptp', 'kan_income_kCHF_pc', 'kan_wealth_Mchf_ptp', 'kan_wealth_Mchf_pc']
    for col in cols_to_transform:
        data[col] = pd.to_numeric(data[col], errors='coerce')
        data['log_' + col] = np.log(data[col])

    data.sort_index(inplace=True)

    return data


###################
# Helper functions
###################

def convert_date_to_datetime(data, date_columns=['andat', 'first_donation', 'last_donation']):
    # Convert string date columns into datetime columns
    for col in date_columns:
        data[col] = pd.to_datetime(data[col], format='%Y-%m-%d')  # .dt.year
    return(data)


def map_datetime_to_decimal(data, datetime_column, inplace=False):
    # Convert datetime columns to decimal columbs
    if inplace:
        data[datetime_column] = data[datetime_column].map(datetime2decimal)
    else:
        data['num_{}'.format(datetime_column)] = data[datetime_column].map(datetime2decimal)
    return data


def datetime2decimal(date):
    """
    Converts dates to decimal numbers

    Parameters
    ----------
    date : datetime

    Returns
    -------
    Float64
        Decimal representation of the date.
    """
    if pd.isna(date):
        return date
    year_part = date - datetime(year=date.year, month=1, day=1)
    year_length = (datetime(year=date.year + 1, month=1, day=1) - datetime(year=date.year, month=1, day=1))
    return date.year + year_part / year_length
