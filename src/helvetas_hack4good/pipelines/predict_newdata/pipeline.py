"""
Predict new dataset pipeline definition
"""

from kedro.pipeline import Pipeline, node
from ..data_engineering.nodes import clean_helvetas_data, enhance_data, create_features
from ..data_science.nodes import feature_selection, relabel_data, predict_test

def create_pipeline(**kwargs):
    return Pipeline(
        [
            node(
                func=clean_helvetas_data,
                inputs=["helvetas_data_new", "params:verbose"],
                outputs="preprocessed_helvetas_new",
                name="cleaning_helvetas_new"
            ),
            node(
                func=enhance_data,
                inputs=["preprocessed_helvetas_new", "transformed_demographics", "transformed_names", "params:verbose"],
                outputs="enhanced_data_new",
                name="enhancing_data_new"
            ),
            node(
                func=create_features,
                inputs=["enhanced_data_new", "params:encoding", "params:missing_action"],
                outputs="features_data_new",
                name="creating_features_new"
            ),
            node(
                func=feature_selection,
                inputs=["features_data_new", "params:from_dataset", "params:feat_list", "params:k", "params:verbose"],
                outputs="model_input_data_new",
                name="selecting_features"
            ),
            node(
                func=relabel_data,
                inputs=["model_input_data_new", "params:verbose"],
                outputs="model_input_relabeled",
                name="relabeling_new"
            ),
            node(
                func=predict_test,
                inputs=["model", "params:model_name", "model_input_relabeled", "params:column_to_predict"],
                outputs=["new_prediction", "model_metrics_new"],
                name="predicting_new"
            )
        ]
    )
