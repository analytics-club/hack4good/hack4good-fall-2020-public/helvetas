# Kedro Info

## Overview

Take a look at the [Kedro documentation](https://kedro.readthedocs.io) to get started.

## How to install kedro

To install Kedro from the Python Package Index (PyPI) simply run:

```
pip install kedro
```

## How to run your Kedro pipeline

You can run your Kedro project with:

```
kedro run
```

> Note: Our project is already structured as a kedro project. First, you should pull the repo to local. Next, you need to add the Helvetas dataset `Helvetas_H4G2020.csv` to the folder `data/01_raw`.


## Kedro pipelines

Nodes are the building blocks of pipelines and represent tasks. Pipelines are used to combine nodes to build machine learning workflows.

### Nodes

A node is created by specifying a Python function, input variable names and output variable names. 
To run a node (python script/shell), you need to instantiate its inputs, you can use:
```
from kedro.pipeline import *
from kedro.io import *
from kedro.runner import *
node_name.run(dict(inputs))
```
or just call it as a regular Python function.

[Further information about nodes](https://kedro.readthedocs.io/en/stable/06_nodes_and_pipelines/01_nodes.html#)

### Pipelines

You can create a new pipeline by running

```
kedro pipeline create <pipeline_name>
```

Each pipeline has its own folder: `src/new_kedro_project/pipelines/pipeline_name`
Inside this folder you can find three important files:
- `pipeline.py` where the pipeline is defined
- `nodes.py` with the nodes (functions) of the pipeline, this is where the code really is
- `README.md` pipeline documentation, description of parameters and functionality

To run just one of the pipelines you can use

```
kedro run --pipeline pipeline_name
```

[Further information about pipelines](https://kedro.readthedocs.io/en/stable/06_nodes_and_pipelines/02_pipelines.html)

> Note: For now we have two pipelines, data engineering (pipeline name is **de**) and data science (**ds**). In the **data engineering pipeline** we have all data preprocessing steps, feature engineering and feature selection (remember to follow the [data engineering convention](https://kedro.readthedocs.io/en/stable/11_faq/01_faq.html#what-is-data-engineering-convention)!). The **data science pipeline** is for the machine learning models.

## Data

- Add datasets to the `data/` folder, according to [data engineering convention](https://kedro.readthedocs.io/en/stable/11_faq/01_faq.html#what-is-data-engineering-convention)
- Register the datasets with the Data Catalog, which is the registry of all data sources available for use by the project `conf/base/catalog.yml`. 

[Further information on the data catalog](https://kedro.readthedocs.io/en/stable/05_data/01_data_catalog.html)

> Note: You can then use the datasets in the catalog in your functions and notebooks directly, using the catalog name.

> Remember to add the Helvetas dataset `Helvetas_H4G2020.csv` to the folder `data/01_raw`

## Parameters

Say you have a set of parameters you’re playing around with for your model. You can declare these in one place, for instance `conf/base/parameters.yml`, so that you isolate your changes to one central location.

You may now reference these parameters in the node definition, using the params: prefix:

```
# in pipeline definition
node(
    func=func_name,
    inputs=["a", "params:p"],
    outputs="b",
)
```

[Further information on parameters](https://kedro.readthedocs.io/en/stable/04_kedro_project_setup/02_configuration.html#parameters)

## How to visualize your pipeline

Kedro-Viz is a really cool way to display data and machine-learning pipelines in an informative way, emphasising the connections between datasets and nodes. It shows the structure of your Kedro pipeline.

You can install Kedro-Viz by running:

```
pip install kedro-viz
```
You should be in your project root directory, and once Kedro-Viz is installed you can visualise your pipeline by running:

```
kedro viz
```

## How to work with Kedro and Jupyter notebooks

To use Jupyter notebooks in your Kedro project, you need to install Jupyter:
> Note:  Jupyter is already included in the project requirements by default, so once you have run `kedro install` you will not need to take any extra steps before you use them.

```
pip install jupyter
```

After installing Jupyter, you can start a local notebook server:

```
kedro jupyter notebook
```

### Loading DataCatalog in Jupyter
You can load a dataset defined in your `conf/base/catalog.yml`, by simply executing the following:

```
df = catalog.load("dataset_name")
```

### Using parameters

Context object also exposes params property, which allows you to easily access all project parameters:

```
parameters = context.params  
parameters["parameter_name"]
```

### How to reload context and catalog variables

You can simply use

```
%reload_kedro
```

### How to convert notebook cells to nodes in a Kedro project
You can move notebook code over into a Kedro project structure using a mixture of [cell tagging](https://jupyter-notebook.readthedocs.io/en/stable/changelog.html#cell-tags) and Kedro CLI commands.

By adding the `node` tag to a cell and running the command below, the cell's source code will be copied over to a Python file within `src/<package_name>/nodes/`:

```
kedro jupyter convert <filepath_to_my_notebook>
```
> *Note:* The name of the Python file matches the name of the original notebook.

Alternatively, you may want to transform all your notebooks in one go. Run the following command to convert all notebook files found in the project root directory and under any of its sub-folders:

```
kedro jupyter convert --all
```

### How to ignore notebook output cells in `git`
To automatically strip out all output cell contents before committing to `git`, you can run `kedro activate-nbstripout`. This will add a hook in `.git/config` which will run `nbstripout` before anything is committed to `git`.

> *Note:* Your output cells will be retained locally.


## Project dependencies

Declare any dependencies in `src/requirements.txt` for `pip` installation and `src/environment.yml` for `conda` installation.

To install them, run:

```
kedro install
```

To generate or update the dependency requirements for your project:

```
kedro build-reqs
```

This will copy the contents of `src/requirements.txt` into a new file `src/requirements.in` which will be used as the source for `pip-compile`. You can see the output of the resolution by opening `src/requirements.txt`.

After this, if you'd like to update your project requirements, please update `src/requirements.in` and re-run `kedro build-reqs`.

[Further information about project dependencies](https://kedro.readthedocs.io/en/stable/04_kedro_project_setup/01_dependencies.html#project-specific-dependencies)


## Package your Kedro project

[Further information about building project documentation and packaging your project](https://kedro.readthedocs.io/en/stable/03_tutorial/05_package_a_project.html)
