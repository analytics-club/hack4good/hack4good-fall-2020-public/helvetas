# How-to Git and Gitlab

For version control of our project we will use Git and Gitlab.

We will have a simple workflow, with the master branch and one branch for each of the team members. Since we all have write access to master, when you finish your current task you can merge your branch with master (no need of pull request).

> IMPORTANT: Do not commit datasets, e.g., Helvetas dataset, or intermediate datasets to the repository (data confidentiality? and storage issue).  *Exception:* external datasets, so we can all have the same version.

## Git workflow
**1. Pull the last changes from master into your branch**

```
git checkout your-branch    
git fetch origin        # gets you up to date with origin, you can use git diff, git log to see the changes
git merge origin/master # copy the changes to your branch
```

Or just in one step

```
git checkout your-branch
git pull origin master # copy the changes to your branch
```

**2. Work, commit and push**

Useful commands

```
git status 
git add .
git commit -m "your commit message"
git push  # to your remote branch
```

> Note: Use meaningful commit messages!

**3. Open a merge request (optional)**

Merge requests (MRs) are optional, but often advantageous. They are a good way to keep your collaborators updated about your progress and collect feedback.
The easiest way to use them is via the Gitlab UI:

Basic steps

- Create a new merge request, *e.g.*, via *Merge requests* -> *New merge request* -> ...
- Select a descriptive title. Start the title with *"WIP:"* or *"Draft:"* if the MR is still a work in progress.
- Describe what is (planned to be) done in this MR in the description.
- Select whether to automatically delete the source branch after the merge (you usually want that).
- Keep working on your branch as usual.
- Under *Changes* the diff between source and target branch is displayed. You can add comments here to, *e.g.*, ask others for feedback.
- Once you are done with your work, make sure that there are no merge conflict between source and target branch. Then resolve the work in progress status.
- Either assign the MR to someone else (top right of the page) or merge yourself by clicking on *Merge*.
> Note: Due to the limited time frame of this project it might often be advantageous to merge yourself.

- If you get assigned a MR by someone else: 
    - Once the MR is no longer a WIP, review the changes. Add comments if there is anything that you think should be changed.
    - As soon as you are fine with the changes, merge the branch by clicking on *Merge*.

See https://docs.gitlab.com/ee/user/project/merge_requests/getting_started.html for further information.


**4. Merge your branch**

```
git checkout master
git pull
git merge your-branch --no-ff -m "your merge message"
```

If any merge conflicts, don't panic, just resolve them. Go to the file with the conflict, edit the file (decide what stays and what goes) and commit.

Nice workflow tutorial [Git feature branch workflow](https://www.atlassian.com/git/tutorials/comparing-workflows/feature-branch-workflow)
