.. helvetas_hack4good documentation master file, created by sphinx-quickstart.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to project helvetas_hack4good's API docs!
=============================================


.. toctree::
   :maxdepth: 4

   modules


To use the Hack4Good-Helvetas pipeline you should have a directory that includes:

* Package wheel file
* *data/01_raw* folder with raw datasets
* *conf/* folder with configuration files

1. Install package and dependencies::

	pip install helvetas_hack4good-0.1-py3-none-any.whl

2. Run pipeline::

	python -m helvetas_hack4good.run

3. The predicted values for the dataset without birth year will be in the file *data/07_model_output/prediction.csv*

To run a different model, you can modify the parameters of the pipeline in *conf/base/parameters.yaml*

If kedro is installed, you can visualize the pipeline::

	kedro viz

And open the MLflow Dashboard::

	kedro mlflow ui 

then open http://127.0.0.1:5000/ in the explorer.



