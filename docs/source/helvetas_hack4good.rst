Helvetas Hack4Good Pipeline
===========================

.. automodule:: helvetas_hack4good.pipelines
    :members:
    :undoc-members:
    :show-inheritance:

Subpackages
-----------

.. toctree::

    helvetas_hack4good.pipelines.data_engineering.rst
    helvetas_hack4good.pipelines.data_science.rst

