Some resources with interesting background info for the Helvetas project:

* Swissfundraising (German/French only):
    * A short [report](https://swissfundraising.org/generationenwechsel-als-herausforderung-fuers-fundraising/) about the effect of generational changes on the donation volume in Switzerland 
    * Some additional [graphs](https://swissfundraising.org/spenderinnengenerationen/) depicting the share of donators within the different generations
* Federal Statistical Office:
    * Some [basic demographic data](https://www.bfs.admin.ch/bfs/en/home/statistics/population/effectif-change/age-marital-status-nationality.html) about Switzerland 
    * First name statistics: For [men](https://www.bfs.admin.ch/bfs/en/home/statistics/population/births-deaths/first-names-switzerland.assetdetail.13707162.html) and [women](https://www.bfs.admin.ch/bfs/en/home/statistics/population/births-deaths/first-names-switzerland.assetdetail.13707161.html) **living** in Switzerland in 2019 including birth year statistics
    * [Population per commune including age resolution](https://www.bfs.admin.ch/bfs/en/home/statistics/catalogues-databases/tables.assetdetail.13707291.html)
* [Open Data Swiss](https://opendata.swiss/en/dataset/statistics-on-swiss-cities-and-towns-2020):
        * Many datasets about [urban regions](https://opendata.swiss/en/dataset/statistics-on-swiss-cities-and-towns-2020) - might also be a nice starting point to distinguish between rural and urban regions
* [Federal taxation office statistics](https://www.estv.admin.ch/estv/de/home/allgemein/steuerstatistiken/fachinformationen/steuerstatistiken.html):
    * [Income and taxation until 2016](https://www.estv.admin.ch/estv/de/home/allgemein/steuerstatistiken/fachinformationen/steuerstatistiken/direkte-bundessteuer.html): includes total income also per municipality
    * [Wealth by canton until 2016](https://www.estv.admin.ch/estv/de/home/allgemein/steuerstatistiken/fachinformationen/steuerstatistiken/gesamtschweizerische-vermoegensstatistik-der-natuerlichen-person.html): They claim that they don't collect the data per municipality
* [Swisspost](https://swisspost.opendatasoft.com/explore/):
    * [Population per PLZ](https://swisspost.opendatasoft.com/explore/dataset/bevoelkerung_proplz/table/?disjunctive.plz&disjunctive.typ&disjunctive.ortbez18&sort=stichdatum) NOTE: Be careful to also consider the "TYP" column: m = male, w=female, h = household, f=business; Assumably lower quality than federal statistics data
    * [Municipality number to postal code conversion](https://swisspost.opendatasoft.com/explore/dataset/plz_verzeichnis_v2/table/) Municipality number from tax info appears to be 
* [ZEWO donation statistics](https://zewo.ch/de/spendenstatistik/): Some interesting background info regarding *e.g.* the time evolution of donations in Switzerland
