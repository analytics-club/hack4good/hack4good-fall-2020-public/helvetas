# Helvetas Hack4Good

## Project Title: Generational Change of Helvetas' Donors
#### Duration: Sept 30 - November 25, 2020
#### Team: 
**Students:** Rodrigo González, Stephanie Grimmel, Jinyan Tao, Cecilia Valenzuela\
**Mentor:** Lionel Trebuchon\
**Hack4Good contact person:** Jaco Fuchs\
**Helvetas contact person:** Bernardo Romero

### What is Hack4Good?

Hack4Good is a 8-week long pro-bono program that matches data science talents from ETH Zurich with non-profit organisations that promote social causes. In close collaboration with such a non-profit, small student teams develop and implement data-driven solutions to increase these organizations' impact.

### What is Helvetas?

Helvetas Swiss Intercooperation is a Swiss non-profit development organisation based in Zürich. As a politically and confessional independent organization, they are committed to improving the livelihoods of poor and disadvantaged people in developing, transition and emerging countries, strengthening local actors and creating institutional frameworks that enable sustainable development.

### Project Context

The Marketing & Communications (MC) department coordinates all fundraising and communication activities of Helvetas. Through numerous online and offline Fundraising campaigns MC raises around 50% of the funds needed to run Helvetas’ projects. These campaigns or fundraising activities may target a large range of individuals who donate a few Swiss Francs only once, to others who donate thousands per year or that include Helvetas as beneficiary of their legacy after their death.

A big challenge for a fundraising organisation is the generational change of its donor base. Studies carried out in Switzerland show how older generations tend to donate much more than younger ones. In particular, the generation that was born during or shortly after World War II are currently still the single largest donor segment. Unfortunately, this generation is rapidly fading away, which raises the challenge of communicating and attracting the donations of following generations like the “Babyboomers”, “Gen Xers” or “Millennials”.

To cope with these challenges, Helvetas needs to first fully understand the generational composition of its donor base. Unfortunately, we only know the birthyear of about 28% of our donors, which makes our analysis insufficient and unreliable. Being able to find out, with a good degree of confidence, the age of the other 72% would represent for a significant improve of our analytical capabilities that would in turn translate into better-crafted fundraising campaigns.

[More info](Helvetas_Project_Proposal.pdf)

### Goals

1. **Prediction of the birthyear of the donors for which no recorded birth date information in the database**\
This is our main goal with this project, and we expect this to be possible by developing a machine learning model that uses as input data the information we have from these donors and external data, combine and use as predictive variables.

2. **Address the generational change question**\
Forecast how the generational composition of the Helvetas donor-base will evolve over the next 20 years with the newly acquired information.\
How would our donors be split by generations in 10 or 20 years?\
What could that mean for the donations perceived?\
How should a fundraising organisation react to these trends?

### Deliverables
TODO
- Complete dataset with predictions
- Model (kedro?)
- Report (who will read it?)
- Presentation Hack4Good

## Organization

### Project Timeline
**| Sept 30**: Kick-off\
**| Oct 1 - Oct 25:** Project planning and Exploratory Data analysis\
**| Oct 25 - ...:** Feature engineering and Feature selection\
**| Oct 25 - ...:** Modeling\
**| Nov ...:** Generational change question\
**|** ...\
**| Nov 18:** Final Presentations\
**| Nov 25:** Final Event\

### Tools
**Project organization -** Kedro [Kedro How-to](howto-kedro.md)\
**Version control -** Git and Gitlab [Git How-to](howto-git.md)\
**Communication -** Slack channel h4g-hs2020-helvetas

